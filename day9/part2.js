const fs = require('fs');
const input = './input/input.txt';
const OpcodeProgram = require('./opcodeProgram');
let results = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const input = contents.toString();

  inputArr = input.split(',').map(function (x) {
    return parseInt(x, 10);
  });

  const program = new OpcodeProgram([...inputArr]);
  const result = program.runProgram([...inputArr], 2);
  console.timeEnd('execute');
  console.log(result);
});