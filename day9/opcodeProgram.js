class OpcodeProgram {
  constructor (input) {
    this.input = input;
    this.pos = 0;
    this.inputSignalCount = 0;
    this.input1 = 0;
    this.input2 = 0;
    this.endProgram = false;
    this.outputs = [];
    this.ended = false;
    this.backAtInput = false;
    this.continueMode = false;
    this.relativeBase = 0;
    this.extMemory = {};
  }

  handleOptBlock(inputArr) {
    let opCode = this.getValueAtPos(this.pos);
    const val1 = this.getValueAtPos(this.pos + 1);
    const val2 = this.getValueAtPos(this.pos + 2);
    const writePos = this.getValueAtPos(this.pos + 3);

    // Parameters mode
    if (opCode > 4 && opCode !== 99) {
      // Read right to left, so reverse the array for easier mode determination
      const modes = opCode.toString().split('').reverse();
      opCode = parseInt(modes[0]);
      const m1 = modes.length > 2 ? parseInt(modes[2]) : 0;
      const m2 = modes.length > 3 ? parseInt(modes[3]) : 0;
      const m3 = modes.length > 4 ? parseInt(modes[4]) : 0;

      // Determine the values and write position based on mode
      const value1 = m1 === 1 ? val1 : m1 === 2 ? this.getValueAtPos(val1 + this.relativeBase) : this.getValueAtPos(val1);
      const value2 = m2 === 1 ? val2 : m2 === 2 ? this.getValueAtPos(val2 + this.relativeBase) : this.getValueAtPos(val2);
      const wp = m3 === 1 ? this.pos + 3 : m3 === 2 ? writePos + this.relativeBase : writePos;

      if (opCode === 1) {
        this.addValues(value1, value2, wp);
      }

      else if (opCode === 2) {
        this.multiplyValues(value1, value2, wp);
      }

      else if (opCode === 3) {
        if (!this.backAtInput) {
          if (this.inputSignalCount === 0) {
            this.setValueAtPos(val1 + this.relativeBase, this.input1);
            this.inputSignalCount++;
          } else {
            this.setValueAtPos(val1 + this.relativeBase, this.input2);
            this.inputSignalCount++
          }

          this.pos = this.pos + 2;

          if (this.continueMode && this.inputSignalCount > 1) {
            this.backAtInput = true;
          }
        } else {
          this.endProgram = true;
        }
      }

      else if (opCode === 4) {
        this.outputs.push(value1);
        this.pos = this.pos + 2;
      }

      // Jump if true
      else if (opCode === 5) {
        this.jumpIfTrue(value1, value2);
      }

      // Jump if false
      else if (opCode === 6) {
        this.jumpIfFalse(value1, value2);
      }

      else if (opCode === 7) {
        this.lessThan(value1, value2, wp);
      }

      else if (opCode === 8) {
        this.equals(value1, value2, wp);
      }

      else if (opCode === 9) {
        this.relativeBase += value1;
        this.pos += 2;
      }
    }

    // Regular Mode
    else {
      const opCode = this.getValueAtPos(this.pos);
      const value1 = this.getValueAtPos(val1);
      const value2 = this.getValueAtPos(val2);
      if (opCode === 1) {
        this.addValues(value1, value2, writePos);
      }

      else if (opCode === 2) {
        this.multiplyValues(value1, value2, writePos);
      }

      else if (opCode === 3) {
        if (!this.backAtInput) {
          if (this.inputSignalCount === 0) {
            this.setValueAtPos(val1, this.input1);
            this.inputSignalCount++;
          } else {
            this.setValueAtPos(val1, this.input2);
            this.inputSignalCount++
          }

          this.pos = this.pos + 2;

          if (this.continueMode && this.inputSignalCount > 1) {
            this.backAtInput = true;
          }
        } else {
          this.endProgram = true;
        }
      }

      else if (opCode === 4) {
        this.outputs.push(value1);
        this.pos = this.pos + 2;
      }

      // Jump if true
      else if (opCode === 5) {
        this.jumpIfTrue(value1, value2);
      }

      // Jump if false
      else if (opCode === 6) {
        this.jumpIfFalse(value1, value2);
      }

      else if (opCode === 7) {
        this.lessThan(value1, value2, writePos);
      }

      else if (opCode === 8) {
        this.equals(value1, value2, writePos);
      }

      else if (opCode === 9) {
        this.relativeBase += value1;
        this.pos += 2;
      }

      if (opCode === 99) {
        this.endProgram = true;
        this.ended = true;
      }
    }
  }

  getValueAtPos (pos) {
    let value;
    if (pos > this.input.length) {
      value = this.extMemory[pos];
      if (value === undefined) {
        this.extMemory[pos] = 0;
        value = 0;
      }
    } else {
      value = this.input[pos];
    }

    return value;
  }

  setValueAtPos(pos, value) {
    if (pos > this.input.length) {
      this.extMemory[pos] = value;
    } else {
      this.input[pos] = value;
    }
  }

  addValues (first, second, wp) {
    const sum = first + second;
    this.setValueAtPos(wp, sum);
    this.pos = this.pos + 4;
  }

  multiplyValues (first, second, wp) {
    const product = first * second;
    this.setValueAtPos(wp, product);
    this.pos = this.pos + 4;
  }

  lessThan (a, b, wp) {
    if (a < b) {
      this.setValueAtPos(wp, 1);
    } else {
      this.setValueAtPos(wp, 0);
    }

    this.pos = this.pos + 4;
  }

  equals (a, b, wp) {
    if (a === b) {
      this.setValueAtPos(wp, 1);
    } else {
      this.setValueAtPos(wp, 0);
    }

    this.pos = this.pos + 4;
  }

  jumpIfTrue (value1, value2) {
    if (value1 !== 0) {
      this.pos = value2;
    } else {
      this.pos = this.pos + 3;
    }
  }

  jumpIfFalse (value1, value2) {
    if (value1 === 0) {
      this.pos = value2
    } else {
      this.pos = this.pos + 3;
    }
  }

  runProgram (input, in1 = 0, in2 = 0, continueMode = false) {
    this.input = input;
    this.input1 = in1;
    this.input2 = in2;
    this.pos = 0;
    this.inputSignalCount = 0;
    this.backAtInput = false;
    this.outputs = [];
    this.endProgram = false;
    this.ended = false;
    this.continueMode = continueMode;

    while (!this.endProgram) {
      this.handleOptBlock();
    }

    return this.outputs;
  }

  continueProgram (inputVal) {
    this.input2 = inputVal;
    this.backAtInput = false;
    this.continueMode = true;
    this.endProgram = false;
    this.outputs = [];

    while (!this.endProgram) {
      this.input = this.handleOptBlock(this.input);
    }

    return [this.outputs, this.ended];
  }
}

module.exports = OpcodeProgram
