const fs = require('fs');
const input = './input/input.txt';
let stored = 0
let pos = 0;
let val1 = 0;
let val2 = 0;
let writePos = 0;
let inputArr = [];
const inputVal = 5;
let outputs = [];

const handleOptBlock = () => {
  val1 = inputArr[pos + 1];
  val2 = inputArr[pos + 2];
  writePos = inputArr[pos + 3];

  // Parameters mode
  if (inputArr[pos] > 4 && inputArr[pos] !== 99) {
    // Read right to left, so reverse the array for easier mode determination
    const modes = inputArr[pos].toString().split('').reverse();
    const opCode = parseInt(modes[0]);
    const m1 = modes.length > 2 ? parseInt(modes[2]) : 0;
    const m2 = modes.length > 3 ? parseInt(modes[3]) : 0;
    const m3 = modes.length > 4 ? parseInt(modes[4]) : 0;

    // Determine the values and write position based on mode
    const value1 = m1 === 1 ? val1 : inputArr[val1];
    const value2 = m2 === 1 ? val2 : inputArr[val2];
    const wp = m3 === 1 ? pos + 3 : writePos;

    if (opCode === 1) {
      addValues(value1, value2, wp);
    }

    else if (opCode === 2) {
      multiplyValues(value1, value2, wp);
    }

    else if (opCode === 4) {
      outputs.push(value1);
      pos = pos + 2;
    }

    // Jump if true
    else if (opCode === 5) {
      jumpIfTrue(value1, value2);
    }

    // Jump if false
    else if (opCode === 6) {
      jumpIfFalse(value1, value2);
    }

    else if (opCode === 7) {
      lessThan(value1, value2, wp);
    }

    else if (opCode === 8) {
      equals(value1, value2, wp);
    }
  }

  // Regular Mode
  else {
    if (inputArr[pos] === 1) {
      addValues(inputArr[val1], inputArr[val2], writePos);
    }

    else if (inputArr[pos] === 2) {
      multiplyValues(inputArr[val1], inputArr[val2], writePos);
    }

    else if (inputArr[pos] === 3) {
      inputArr[val1] = inputVal;
      pos = pos + 2;
    }

    else if (inputArr[pos] === 4) {
      outputs.push(inputArr[val1]);
      pos = pos + 2;
    }

    // Jump if true
    else if (inputArr[pos] === 5) {
      jumpIfTrue(inputArr[val1], inputArr[val2]);
    }

    // Jump if false
    else if (inputArr[pos] === 6) {
      jumpIfFalse(inputArr[val1], inputArr[val2]);
    }

    else if (inputArr[pos] === 7) {
      lessThan(inputArr[val1], inputArr[val2], writePos);
    }

    else if (inputArr[pos] === 8) {
      equals(inputArr[val1], inputArr[val2], writePos);
    }

    if (inputArr[pos] === 99) {
      console.log(outputs);
      console.log(`Final output: ${outputs[outputs.length - 1]}`);
      console.timeEnd('execute');
      process.exit(0);
    }
  }
}

const addValues = (first, second, wp) => {
  const sum = first + second;
  inputArr[wp] = sum;
  pos = pos + 4;
}

const multiplyValues = (first, second, wp) => {
  const product = first * second;
  inputArr[wp] = product;
  pos = pos + 4;
}

const lessThan = (a, b, wp) => {
  if (a < b) {
    inputArr[wp] = 1;
  } else {
    inputArr[wp] = 0;
  }

  pos = pos + 4;
}

const equals = (a, b, wp) => {
  if (a === b) {
    inputArr[wp] = 1;
  } else {
    inputArr[wp] = 0;
  }

  pos = pos + 4;
}

const jumpIfTrue = (value1, value2) => {
  if (value1 !== 0) {
    pos = value2;
  } else {
    pos = pos + 3;
  }
}

const jumpIfFalse = (value1, value2) => {
  if (value1 === 0) {
    pos = value2
  } else {
    pos = pos + 3;
  }
}

console.time('execute');

fs.readFile(input, 'utf8', function(err, contents) {
  // create arrays
  const input = contents.toString();

  inputArr = input.split(',').map(function (x) {
    return parseInt(x, 10);
  });

  while (pos < inputArr.length) {
    handleOptBlock();
  }
});
