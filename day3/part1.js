const fs = require('fs');
const input = './input/input.txt';
const xi = 0;
const yi = 0;
let points = {};
let xCur = 0;
let yCur = 0;
let sharedPoints = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  // create arrays
  const resultArr = contents.toString().split("\n");
  let wire1 = resultArr[0];
  const wire1Arr = wire1.split(',');

  let wire2 = resultArr[1];
  const wire2Arr = wire2.split(',');

  // Get all the points for the first wire
  for (let i = 0; i < wire1Arr.length; i++) {
    calcPoints(wire1Arr[i], 1);
  }

  // Get all the points for the second wire
  xCur = 0;
  yCur = 0;
  for (let i = 0; i < wire2Arr.length; i++) {
    calcPoints(wire2Arr[i], 2);
  }
  points = Object.keys(sharedPoints);
  console.log(points);

  let smallestDistance = 999999;
  points.forEach(point => {
    let pointArr = point.split(',');
    let x1 = parseInt(pointArr[0]);
    let y1 = parseInt(pointArr[1]);
    let dist = Math.abs(xi - x1) + Math.abs(yi - y1);
    if (dist < smallestDistance && dist > 0) {
      smallestDistance = dist;
    }
  });
  console.timeEnd('execute');
  console.log("Smallest Distance: "+smallestDistance);
});

const calcPoints = (input, set) => {
  let direction = input.charAt(0);
  let distance = parseInt(input.substring(1, input.length));

  if (direction === 'R') {
    while (distance > 0) {
      let newX = ++xCur;
      points[`${newX},${yCur}`] === undefined ? points[`${newX},${yCur}`] = set
      : points[`${newX},${yCur}`] !== set ? sharedPoints[`${newX},${yCur}`] = 1 : null;
      distance--;
    }
  }
  if (direction === 'L') {
    while (distance > 0) {
      let newX = --xCur;
      points[`${newX},${yCur}`] === undefined ? points[`${newX},${yCur}`] = set
      : points[`${newX},${yCur}`] !== set ? sharedPoints[`${newX},${yCur}`] = 1 : null;
      distance--;
    }
  }
  if (direction === 'U') {
    while (distance > 0) {
      let newY = --yCur;
      points[`${xCur},${newY}`] === undefined ? points[`${xCur},${newY}`] = set
      : points[`${xCur},${newY}`] !== set ? sharedPoints[`${xCur},${newY}`] = 1 : null;
      distance--;
    }
  }
  if (direction === 'D') {
    while (distance > 0) {
      let newY = ++yCur;
      points[`${xCur},${newY}`] === undefined ? points[`${xCur},${newY}`] = set
      : points[`${xCur},${newY}`] !== set ? sharedPoints[`${xCur},${newY}`] = 1 : null;
      distance--;
    }
  }
}

