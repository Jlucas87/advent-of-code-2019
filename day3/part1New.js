const fs = require('fs');
const input = './input/input.txt';
const xi = 0;
const yi = 0;
let xCur = 0;
let yCur = 0;
let lines1 = [];
let lines2 = [];
let sharedPoints = [];

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  // create arrays
  const resultArr = contents.toString().split("\n");
  let wire1 = resultArr[0];
  const wire1Arr = wire1.split(',');

  let wire2 = resultArr[1];
  const wire2Arr = wire2.split(',');

  // Get all the points for the first wire
  for (let i = 0; i < wire1Arr.length; i++) {
    calcPoints(wire1Arr[i], 1, lines1);
  }

  // Get all the points for the second wire
  xCur = 0;
  yCur = 0;
  for (let i = 0; i < wire2Arr.length; i++) {
    calcPoints(wire2Arr[i], 2, lines2);
  }

  for (let i = 0; i < lines1.length; i++) {
    for (let j = 0; j < lines2.length; j++) {
      compareLines(lines1[i], lines2[j]);
    }
  }

  let smallestDistance = 999999;
  sharedPoints.forEach(point => {
    let pointArr = point.split(',');
    let x1 = parseInt(pointArr[0]);
    let y1 = parseInt(pointArr[1]);
    let dist = Math.abs(xi - x1) + Math.abs(yi - y1);
    if (dist < smallestDistance && dist > 0) {
      smallestDistance = dist;
    }
  });
  console.timeEnd('execute');
  console.log("Smallest Distance: "+smallestDistance);
});

const calcPoints = (input, set, lines) => {
  let direction = input.charAt(0);
  let distance = parseInt(input.substring(1, input.length));

  if (direction === 'R') {
    let newX = xCur + distance;
    let currentLine = {
      start: [xCur, yCur],
      end: [newX, yCur],
      dir: 'R'
    };
    xCur = newX;
    lines.push(currentLine);
  }
  if (direction === 'L') {
    let newX = xCur - distance;
    let currentLine = {
      start: [xCur, yCur],
      end: [newX, yCur],
      dir: 'L'
    };
    xCur = newX;
    lines.push(currentLine);
  }
  if (direction === 'U') {
    let newY = yCur + distance;
    let currentLine = {
      start: [xCur, yCur],
      end: [xCur, newY],
      dir: 'U'
    };
    yCur = newY;
    lines.push(currentLine);
  }
  if (direction === 'D') {
    let newY = yCur - distance;
    let currentLine = {
      start: [xCur, yCur],
      end: [xCur, newY],
      dir: 'D'
    };
    yCur = newY;
    lines.push(currentLine);
  }
}

const compareLines = (line1, line2) => {
  const l1xs = line1['start'][0];
  const l1xe = line1['end'][0];
  const l1ys = line1['start'][1];
  const l1ye = line1['end'][1]
  const l2xs = line2['start'][0];
  const l2xe = line2['end'][0];
  const l2ys = line2['start'][1];
  const l2ye = line2['end'][1];
  let line1Dir = line1['dir'];
  let line2Dir = line2['dir'];

  // Are the lines parallel, if so, intersection is not possible
  if ((l1xs === l1xe && l2xs === l2xe) || (l1ys === l1ye && l2ys === l2ye)) {
    return false;
  }

  // If the lines are perpendicular, determine if they intersect
  let xInter = 0;
  let yInter = 0;
  if (line1Dir === 'R') {
    if (l2xs > l1xs && l2xs < l1xe) {
      if (line2Dir === 'U') {
        if (l1ys < l2ye && l1ys > l2ys) {
          yInter = l1ys;
          xInter = l2xs;
        }
      } else if (line2Dir === 'D') {
        if (l1ys < l2ys && l1ys > l2ye) {
          yInter = l1ys;
          xInter = l2xs;
        }
      }
    }
  }

  if (line1Dir === 'L') {
    if (l2xs < l1xs && l2xs > l1xe) {
      if (line2Dir === 'U') {
        if (l1ys < l2ye && l1ys > l2ys) {
          yInter = l1ys;
          xInter = l2xs;
        }
      } else if (line2Dir === 'D') {
        if (l1ys < l2ys && l1ys > l2ye) {
          yInter = l1ys;
          xInter = l2xs;
        }
      }
    }
  }

  if (line1Dir === 'U') {
    if (l2ys < l1ye && l2ys > l1ys) {
      if (line2Dir === 'L') {
        if (l1xs > l2xe && l1xs < l2xs) {
          yInter = l2ys;
          xInter = l1xs;
        }
      } else if (line2Dir === 'R') {
        if (l1xs < l2xe && l1xs > l2xs) {
          yInter = l2ys;
          xInter = l1xs;
        }
      }
    }
  }

  if (line1Dir === 'D') {
    if (l2ys > l1ye && l2ys < l1ys) {
      if (line2Dir === 'L') {
        if (l1xs > l2xe && l1xs < l2xs) {
          yInter = l2ys;
          xInter = l1xs;
        }
      } else if (line2Dir === 'R') {
        if (l1xs < l2xe && l1xs > l2xs) {
          yInter = l2ys;
          xInter = l1xs;
        }
      }
    }
  }

  if (xInter !== 0 && yInter !== 0) {
    sharedPoints.push(`${xInter},${yInter}`);
  }
}

