const fs = require('fs');
const input = './input/input.txt';
const xi = 0;
const yi = 0;
let points = {}
let xCur = 0;
let yCur = 0;
let sharedPoints = {};
let dist = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  // create arrays
  const resultArr = contents.toString().split("\n");
  let wire1 = resultArr[0];
  const wire1Arr = wire1.split(',');

  let wire2 = resultArr[1];
  const wire2Arr = wire2.split(',');

  // Get all the points for the first wire
  for (let i = 0; i < wire1Arr.length; i++) {
    calcPoints(wire1Arr[i], 1);
  }
  let points1 = Object.assign({}, points)
  // Get all the points for the second wire
  xCur = 0;
  yCur = 0;
  dist = 0;
  for (let i = 0; i < wire2Arr.length; i++) {
    calcPoints(wire2Arr[i], 2);
  }

  let keys = Object.keys(sharedPoints);
  let distanceSums = [];
  keys.forEach(key => {
    let distA = points1[key];
    let distB = sharedPoints[key];
    distanceSums.push(distA['dist'] + distB['dist']);
  })

  console.timeEnd('execute');
  distanceSums.sort((a, b) => a - b);
  console.log(distanceSums);
  console.log("Smallest Distance: "+distanceSums[0]);
});

const calcPoints = (input, set) => {
  let direction = input.charAt(0);
  let distance = parseInt(input.substring(1, input.length));

  if (direction === 'R') {
    while (distance > 0) {
      let newX = ++xCur;
      dist++;
      points[`${newX},${yCur}`] === undefined ? points[`${newX},${yCur}`] = {set, dist}
      : points[`${newX},${yCur}`]['set'] !== set ? sharedPoints[`${newX},${yCur}`] = {set, dist} : null;
      distance--;
    }
  }
  if (direction === 'L') {
    while (distance > 0) {
      let newX = --xCur;
      dist++;
      points[`${newX},${yCur}`] === undefined ? points[`${newX},${yCur}`] = {set, dist}
      : points[`${newX},${yCur}`]['set'] !== set ? sharedPoints[`${newX},${yCur}`] = {set, dist} : null;
      distance--;
    }
  }
  if (direction === 'U') {
    while (distance > 0) {
      let newY = --yCur;
      dist++;
      points[`${xCur},${newY}`] === undefined ? points[`${xCur},${newY}`] = {set, dist}
      : points[`${xCur},${newY}`]['set'] !== set ? sharedPoints[`${xCur},${newY}`] = {set, dist} : null;
      distance--;
    }
  }
  if (direction === 'D') {
    while (distance > 0) {
      let newY = ++yCur;
      dist++;
      points[`${xCur},${newY}`] === undefined ? points[`${xCur},${newY}`] = {set, dist}
      : points[`${xCur},${newY}`]['set'] !== set ? sharedPoints[`${xCur},${newY}`] = {set, dist} : null;
      distance--;
    }
  }
}

