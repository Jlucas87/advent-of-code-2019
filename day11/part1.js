const fs = require('fs');
const input = './input/input.txt';
const OpcodeProgram = require('./opcodeProgram');
let panels = {};
let robot = {
  currentPos: {
    x: 0,
    y: 0
  },
  facing: 'up'
};
let timesPainted = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const input = contents.toString();

  inputArr = input.split(',').map(function (x) {
    return parseInt(x, 10);
  });
  const program = new OpcodeProgram([...inputArr]);

  let loopEnded = false;
  let paint = 1;
  let output = program.runProgram(inputArr, paint, true);
  let colorInput = moveRobot(output);

  while (!loopEnded) {
    output = program.continueProgram(colorInput);
    if (output[1]) {
      loopEnded = true;
    }

    colorInput = moveRobot(output[0]);
  }

  // Once we finish the robot, let's look at the message
  const keys = Object.keys(panels);
  let coords = {};
  let lowestCoord = 9999;
  let highestCoord = -9999;
  keys.forEach(key => {
    const coord = key.split(',');
    if (coords[coord[1]] === undefined) {
      coords[coord[1]] = [];
    }

    coords[coord[1]].push(coord[0]);
    if (parseInt(coord[0]) < lowestCoord) {
      lowestCoord = parseInt(coord[0]);
    }
    if (parseInt(coord[0]) > highestCoord) {
      highestCoord = parseInt(coord[0]);
    }
  });

  const coordKeys = Object.keys(coords);
  coordKeys.forEach(key => {
    const length = coords[key].length;
    const sortedCoords = coords[key].sort((a, b) => a - b)

    let rowString = '';
    // Add any missing columns to the front
    let lowest = lowestCoord;
    while (parseInt(sortedCoords[0]) > lowest) {
      rowString = rowString.concat('.');
      lowest++;
    }

    // Add the known items
    sortedCoords.forEach(c => {
      if (panels[`${c},${key}`]['color'] === '.') {
        rowString = rowString.concat('.');
      } else {
        rowString = rowString.concat('#');
      }
    });

    // Add any missing columns to the end
    let highest = highestCoord;
    while (parseInt(sortedCoords[sortedCoords.length - 1]) < highest) {
      rowString = rowString.concat('.');
      highest--;
    }
    console.log(rowString);
  });

  console.timeEnd('execute');
});

const moveRobot = (output) => {
  const pos = robot.currentPos;
  const facing = robot.facing;
  const color = output[0];
  const direction = output[1];

  // Paint the panel
  if (panels[`${pos.x},${pos.y}`] === undefined) {
    panels[`${pos.x},${pos.y}`] = {};
    panels[`${pos.x},${pos.y}`]['painted'] = true;
    timesPainted++;
  }
  panels[`${pos.x},${pos.y}`]['color'] = color === 0 ? '.' : '#';

  // Move the robot, 0 rotates it left and 1 rotates it right
  if (facing === 'up') {
    if (direction === 0) {
      robot.facing = 'left';
      robot.currentPos = {
        x: pos.x - 1,
        y: pos.y
      }
    } else {
      robot.facing = 'right';
      robot.currentPos = {
        x: pos.x + 1,
        y: pos.y
      }
    }
  }

  if (facing === 'right') {
    if (direction === 0) {
      robot.facing = 'up';
      robot.currentPos = {
        x: pos.x,
        y: pos.y + 1
      }
    } else {
      robot.facing = 'down';
      robot.currentPos = {
        x: pos.x,
        y: pos.y - 1
      }
    }
  }

  if (facing === 'down') {
    if (direction === 0) {
      robot.facing = 'right';
      robot.currentPos = {
        x: pos.x + 1,
        y: pos.y
      }
    } else {
      robot.facing = 'left';
      robot.currentPos = {
        x: pos.x - 1,
        y: pos.y
      }
    }
  }

  if (facing === 'left') {
    if (direction === 0) {
      robot.facing = 'down';
      robot.currentPos = {
        x: pos.x,
        y: pos.y - 1
      }
    } else {
      robot.facing = 'up';
      robot.currentPos = {
        x: pos.x,
        y: pos.y + 1
      }
    }
  }

  let newColor = 0;
  let curBotPos = `${robot.currentPos.x},${robot.currentPos.y}`;
  if (panels[curBotPos] !== undefined) {
    if (panels[curBotPos]['color'] === '#') {
      newColor = 1;
    }
  }

  return newColor;
}