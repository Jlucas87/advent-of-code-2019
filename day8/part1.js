const fs = require('fs');
const input = './input/input.txt';
const width = 25;
const height = 6;
const layers = [];
const layerDigits = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const layerSize = width * height;
  let pos = 0;

  // Find each of the layers and add to an array
  while (contents.length > pos) {
    const layer = contents.substring(pos, layerSize + pos);
    layers.push(layer);
    pos += layerSize;
  }

  // Find the layer with the fewest zeros
  let i = 0;
  layers.forEach(layer => {
    const zeros = (layer.match(new RegExp("0", "g")) || []).length;
    layerDigits[zeros] = i;
    i++;
  });
  const keys = Object.keys(layerDigits);
  const noOfZeros = keys[0];
  const targetLayer = layerDigits[noOfZeros];

  const ones = (layers[targetLayer].match(new RegExp("1", "g")) || []).length;
  const twos = (layers[targetLayer].match(new RegExp("2", "g")) || []).length;
  console.log('Final Result: '+(ones*twos));
});