const fs = require('fs');
const input = './input/input.txt';
const width = 25;
const height = 6;
const layers = [];
const layerDigits = {};
let message = '';

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const layerSize = width * height;
  let pos = 0;

  // Find each of the layers and add to an array
  while (contents.length > pos) {
    const layer = contents.substring(pos, layerSize + pos);
    layers.push(layer);
    pos += layerSize;
  }

  // Decode the image
  for (let i = 0; i < layerSize; i++) {
    for (let j = 0; j < layers.length; j++) {
      if (layers[j][i] === '0') {
        message = message.concat(' ');
        j = layers.length;
      } else if (layers[j][i] === '1') {
        message = message.concat('X');
        j = layers.length;
      }
    }
  }

  let messageRows = [];
  pos = 0;
  while (message.length > pos) {
    const row = message.substring(pos, width + pos);
    console.log(row);
    pos += width;
  }

});