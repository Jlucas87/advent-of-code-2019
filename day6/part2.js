const fs = require('fs');
const input = './input/input.txt';
let orbits = {};
let root = null;
let totalChildren = 0;
let queue = [];
let allObjects = [];

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {

  const resultArr = contents.toString().split("\n");
  const tree = new Tree();
  const root = 'COM';
  tree.add(root);

  for (let i = 0; i < resultArr.length; i++) {
    const orbitArr = resultArr[i].split(')');
    const parent = orbitArr[0];
    const child = orbitArr[1];

    if (orbits[parent] === undefined) {
      orbits[parent] = [child];
    } else {
      orbits[parent].push(child);
    }
  }

  // Initialize queue with children of COM
  allObjects.push('COM');
  orbits['COM'].forEach(value => {
    queue.push(value);
    tree.add(value, 'COM');
    allObjects.push(value);
  });

  // Go through each item and add it to the tree
  while (queue.length) {
    let next = queue.shift();

    if (orbits[next] !== undefined) {
      orbits[next].forEach(value => {
        queue.push(value);
        tree.add(value, next);
        allObjects.push(value);
      })
    }
  }
  const SAN = tree.findBFS('SAN');
  const YOU = tree.findBFS('YOU');

  const result = tree.findCommonRootNode(SAN.parent, YOU.parent)
  console.timeEnd('execute');
  console.log(result);
});

function Tree() {
  this.root = null;
}

function Node(data, parent = null) {
  this.parent = parent;
  this.data = data;
  this.children = [];
}

Tree.prototype.add = function(data, searchParent) {
  const parent = searchParent ? this.findBFS(searchParent) : null;

  if (parent) {
    const node = new Node(data, parent)
    parent.children.push(node);
  } else {
    if (!this.root) {
      const node = new Node(data)
      this.root = node;
    }
  }
}

Tree.prototype.contains = function(data) {
  return this.findBFS(data) ? true : false;
};

Tree.prototype.findBFS = function(data, root = null) {
  let queue = [this.root];
  if (root) {
    queue = [root];
  }
  while(queue.length) {
    let node = queue.shift();

    if (node.data === data) {
      return node;
    }

    for(let i = 0; i < node.children.length; i++) {
      queue.push(node.children[i]);
    }
  }

  return null;
}

Tree.prototype.countChildren = function(data) {
  const root = this.findBFS(data);
  let queue = [root];
  let total = 0;
  while (queue.length) {
    let node = queue.shift();

    for(let i = 0; i < node.children.length; i++) {
      total++;
      queue.push(node.children[i]);
    }
  }

  return total;
}

Tree.prototype.findCommonRootNode = function(nodeA, nodeB) {
  let i = 0;
  let parentsA = [];
  let parentsB = [];
  let foundA = false;
  let foundB = false;
  let currentA = nodeA;
  let currentB = nodeB;
  const a = nodeA.data;
  const b = nodeB.data;

  while(!(foundA && foundB)) {
    let parentA = currentA.parent;
    let parentB = currentB.parent;

    if (!foundB) {
      parentsA.push(parentA);
      currentA = parentA;
      const searchB = this.findBFS(b, parentA);
      if (searchB !== null) {
        foundB = true;
      }
    }

    if (!foundA) {
      parentsB.push(parentB);
      currentB = parentB;
      const searchA = this.findBFS(a, parentB);
      if (searchA !== null) {
        foundA = true;
      }
    }
  }

  return parentsA.length + parentsB.length;
}
