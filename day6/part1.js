const fs = require('fs');
const input = './input/input.txt';
let orbits = {};
let root = null;
let totalChildren = 0;
let queue = [];
let allObjects = [];

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {

  const resultArr = contents.toString().split("\n");
  const tree = new Tree();
  const root = 'COM';
  tree.add(root);

  for (let i = 0; i < resultArr.length; i++) {
    const orbitArr = resultArr[i].split(')');
    const parent = orbitArr[0];
    const child = orbitArr[1];

    if (orbits[parent] === undefined) {
      orbits[parent] = [child];
    } else {
      orbits[parent].push(child);
    }
  }

  // Initialize queue with children of COM
  allObjects.push('COM');
  orbits['COM'].forEach(value => {
    queue.push(value);
    tree.add(value, 'COM');
    allObjects.push(value);
  })

  // Go through each item and add it to the tree
  while (queue.length) {
    let next = queue.shift();

    if (orbits[next] !== undefined) {
      orbits[next].forEach(value => {
        queue.push(value);
        tree.add(value, next);
        allObjects.push(value);
      })
    }
  }

  // Go through each unique object in our tree and count the children to get all unique orbits and sub-orbits
  allObjects.forEach(object => {
    totalChildren += tree.countChildren(object);
  })
  console.timeEnd('execute');
  console.log(totalChildren);
});

function Tree() {
  this.root = null;
}

function Node(data) {
  this.data = data;
  this.children = [];
}

Tree.prototype.add = function(data, searchParent) {
  const node = new Node(data)
  const parent = searchParent ? this.findBFS(searchParent) : null;

  if (parent) {
    parent.children.push(node);
  } else {
    if (!this.root) {
      this.root = node;
    }
  }
}

Tree.prototype.contains = function(data) {
  return this.findBFS(data) ? true : false;
};

Tree.prototype.findBFS = function(data) {
  let queue = [this.root];
  while(queue.length) {
    let node = queue.shift();

    if (node.data === data) {
      return node;
    }

    for(let i = 0; i < node.children.length; i++) {
      queue.push(node.children[i]);
    }
  }

  return null;
}

Tree.prototype.countChildren = function(data) {
  const root = this.findBFS(data);
  let queue = [root];
  let total = 0;
  while (queue.length) {
    let node = queue.shift();

    for(let i = 0; i < node.children.length; i++) {
      total++;
      queue.push(node.children[i]);
    }
  }

  return total;
}