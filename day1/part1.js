const fs = require('fs');
const input = './input/input.txt';
let sum = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const resultArr = contents.toString().split("\n");
  resultArr.forEach((mass) => {
    let newMass = mass / 3;
    newMass = Math.floor(newMass);
    newMass = newMass - 2;

    sum += newMass;
  })

  console.log(sum);
  console.timeEnd('execute');
});
