const fs = require('fs');
const input = './input/input.txt';
let sum = 0;

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const resultArr = contents.toString().split("\n");
  resultArr.forEach((mass) => {
    let newMass = Math.floor(mass / 3) - 2;

    let addMass = newMass;
    while (addMass > 0) {
      addMass = addMass / 3;
      addMass = Math.floor(addMass);
      addMass = addMass - 2;
      if (addMass > 0) {
        newMass += addMass;
      }
    }

    sum += newMass;
  })

  console.log(sum);
  console.timeEnd('execute');
});
