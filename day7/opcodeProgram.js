class OpcodeProgram {
  constructor (input) {
    this.input = input;
    this.pos = 0;
    this.inputSignalCount = 0;
    this.input1 = 0;
    this.input2 = 0;
    this.endProgram = false;
    this.outputs = [];
    this.ended = false;
    this.backAtInput = false;
    this.continueMode = false;
  }

  handleOptBlock(inputArr) {
    const val1 = inputArr[this.pos + 1];
    const val2 = inputArr[this.pos + 2];
    const writePos = inputArr[this.pos + 3];

    // Parameters mode
    if (inputArr[this.pos] > 4 && inputArr[this.pos] !== 99) {
      // Read right to left, so reverse the array for easier mode determination
      const modes = inputArr[this.pos].toString().split('').reverse();
      const opCode = parseInt(modes[0]);
      const m1 = modes.length > 2 ? parseInt(modes[2]) : 0;
      const m2 = modes.length > 3 ? parseInt(modes[3]) : 0;
      const m3 = modes.length > 4 ? parseInt(modes[4]) : 0;

      // Determine the values and write position based on mode
      const value1 = m1 === 1 ? val1 : inputArr[val1];
      const value2 = m2 === 1 ? val2 : inputArr[val2];
      const wp = m3 === 1 ? this.pos + 3 : writePos;

      if (opCode === 1) {
        inputArr = this.addValues(value1, value2, wp, inputArr);
      }

      else if (opCode === 2) {
        inputArr = this.multiplyValues(value1, value2, wp, inputArr);
      }

      else if (opCode === 4) {
        this.outputs.push(value1);
        this.pos = this.pos + 2;
      }

      // Jump if true
      else if (opCode === 5) {
        this.jumpIfTrue(value1, value2);
      }

      // Jump if false
      else if (opCode === 6) {
        this.jumpIfFalse(value1, value2);
      }

      else if (opCode === 7) {
        inputArr = this.lessThan(value1, value2, wp, inputArr);
      }

      else if (opCode === 8) {
        inputArr = this.equals(value1, value2, wp, inputArr);
      }
    }

    // Regular Mode
    else {
      if (inputArr[this.pos] === 1) {
        inputArr = this.addValues(inputArr[val1], inputArr[val2], writePos, inputArr);
      }

      else if (inputArr[this.pos] === 2) {
        inputArr = this.multiplyValues(inputArr[val1], inputArr[val2], writePos, inputArr);
      }

      else if (inputArr[this.pos] === 3) {
        if (!this.backAtInput) {
          if (this.inputSignalCount === 0) {
            inputArr[val1] = this.input1;
            this.inputSignalCount++;
          } else {
            inputArr[val1] = this.input2;
            this.inputSignalCount++
          }

          this.pos = this.pos + 2;

          if (this.continueMode && this.inputSignalCount > 1) {
            this.backAtInput = true;
          }
        } else {
          this.endProgram = true;
        }
      }

      else if (inputArr[this.pos] === 4) {
        this.outputs.push(inputArr[val1]);
        this.pos = this.pos + 2;
      }

      // Jump if true
      else if (inputArr[this.pos] === 5) {
        this.jumpIfTrue(inputArr[val1], inputArr[val2]);
      }

      // Jump if false
      else if (inputArr[this.pos] === 6) {
        this.jumpIfFalse(inputArr[val1], inputArr[val2]);
      }

      else if (inputArr[this.pos] === 7) {
        inputArr = this.lessThan(inputArr[val1], inputArr[val2], writePos, inputArr);
      }

      else if (inputArr[this.pos] === 8) {
        inputArr = this.equals(inputArr[val1], inputArr[val2], writePos, inputArr);
      }

      if (inputArr[this.pos] === 99) {
        this.endProgram = true;
        this.ended = true;
      }
    }

    return inputArr;
  }

  addValues (first, second, wp, inputArr) {
    const sum = first + second;
    inputArr[wp] = sum;
    this.pos = this.pos + 4;
    return inputArr;
  }

  multiplyValues (first, second, wp, inputArr) {
    const product = first * second;
    inputArr[wp] = product;
    this.pos = this.pos + 4;
    return inputArr;
  }

  lessThan (a, b, wp, inputArr) {
    if (a < b) {
      inputArr[wp] = 1;
    } else {
      inputArr[wp] = 0;
    }

    this.pos = this.pos + 4;
    return inputArr;
  }

  equals (a, b, wp, inputArr) {
    if (a === b) {
      inputArr[wp] = 1;
    } else {
      inputArr[wp] = 0;
    }

    this.pos = this.pos + 4;
    return inputArr;
  }

  jumpIfTrue (value1, value2) {
    if (value1 !== 0) {
      this.pos = value2;
    } else {
      this.pos = this.pos + 3;
    }
  }

  jumpIfFalse (value1, value2) {
    if (value1 === 0) {
      this.pos = value2
    } else {
      this.pos = this.pos + 3;
    }
  }

  runProgram (input, in1, in2 = 0, continueMode = false) {
    this.input = input;
    this.input1 = in1;
    this.input2 = in2;
    this.pos = 0;
    this.inputSignalCount = 0;
    this.backAtInput = false;
    this.outputs = [];
    this.endProgram = false;
    this.ended = false;
    this.continueMode = continueMode;

    while (!this.endProgram) {
      input = this.handleOptBlock(input);
    }

    return this.outputs;
  }

  continueProgram (inputVal) {
    this.input2 = inputVal;
    this.backAtInput = false;
    this.continueMode = true;
    this.endProgram = false;
    this.outputs = [];

    while (!this.endProgram) {
      this.input = this.handleOptBlock(this.input);
    }

    return [this.outputs, this.ended];
  }
}

module.exports = OpcodeProgram
