const fs = require('fs');
const input = './input/input.txt';
const OpcodeProgram = require('./opcodeProgram');
let permutations = [];
let results = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const input = contents.toString();

  inputArr = input.split(',').map(function (x) {
    return parseInt(x, 10);
  });

  const amplifierA = new OpcodeProgram([...inputArr]);
  const amplifierB = new OpcodeProgram([...inputArr]);
  const amplifierC = new OpcodeProgram([...inputArr]);
  const amplifierD = new OpcodeProgram([...inputArr]);
  const amplifierE = new OpcodeProgram([...inputArr]);

  // Generate all permutations
  const perms = getAllPermutations('56789');

  perms.forEach(perm => {
    const permArr = perm.split('');

    // Run the initial instance of the program
    const valA = amplifierA.runProgram([...inputArr], parseInt(permArr[0]), 0, true);
    const valB = amplifierB.runProgram([...inputArr], parseInt(permArr[1]), valA[0], true);
    const valC = amplifierC.runProgram([...inputArr], parseInt(permArr[2]), valB[0], true);
    const valD = amplifierD.runProgram([...inputArr], parseInt(permArr[3]), valC[0], true);
    const valE = amplifierE.runProgram([...inputArr], parseInt(permArr[4]), valD[0], true);

    // Enter feedback loop mode until ampE quits
    let loopEnded = false;
    let val = [valE];
    let iterations = 0;
    while (!loopEnded) {
      const valCA = amplifierA.continueProgram(val[0][0]);
      const valCB = amplifierB.continueProgram(valCA[0][0]);
      const valCC = amplifierC.continueProgram(valCB[0][0]);
      const valCD = amplifierD.continueProgram(valCC[0][0]);
      val = amplifierE.continueProgram(valCD[0][0]);

      if (val[1]) {
        results[val[0][0]] = perm;
        loopEnded = true;
      }
      iterations++;
    }
  })
  const keys = Object.keys(results);
  const finalValue = keys[keys.length - 1];
  const finalPerm = results[finalValue];
  console.timeEnd('execute');
  console.log(`The permutation: ${finalPerm} produced a signal of ${finalValue}`);
});

function getAllPermutations(string) {
  var results = [];

  if (string.length === 1) {
    results.push(string);
    return results;
  }

  for (let i = 0; i < string.length; i++) {
    const firstChar = string[i];
    let charsLeft = string.substring(0, i) + string.substring(i + 1);
    let innerPermutations = getAllPermutations(charsLeft);
    for (let j = 0; j < innerPermutations.length; j++) {
      results.push(firstChar + innerPermutations[j]);
    }
  }

  return results;
}
