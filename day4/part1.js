const start = 264793;
const end = 803935;
let validPasswordChecker = 0;

const criteriaChecker = (value) => {
  let repeatChecker = false;
  let increaseChecker = true;

  // Is each number increasing or equal to the previous number
  const strValue = value.toString();
  const valueArr = strValue.split('');
  for(let i = 0; i < valueArr.length - 1; i++) {
    const first = parseInt(valueArr[i]);
    const second = parseInt(valueArr[i + 1]);

    // Ensure each subsequent number if equal or greater
    if (second < first) {
      increaseChecker = false;
      break;
    }
    // Look for a repeat
    if (first === second) {
      repeatChecker = true;
    }
  }

  if (repeatChecker && increaseChecker) {
    validPasswordChecker++;
  }
}
console.time('execute');
for (let i = start; i <= end; i++) {
  criteriaChecker(i);
}
console.timeEnd('execute');
console.log(validPasswordChecker);
