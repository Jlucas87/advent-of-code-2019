const input = '1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,6,27,1,27,5,31,2,6,31,35,1,5,35,39,2,39,9,43,1,43,5,47,1,10,47,51,1,51,6,55,1,55,10,59,1,59,6,63,2,13,63,67,1,9,67,71,2,6,71,75,1,5,75,79,1,9,79,83,2,6,83,87,1,5,87,91,2,6,91,95,2,95,9,99,1,99,6,103,1,103,13,107,2,13,107,111,2,111,10,115,1,115,6,119,1,6,119,123,2,6,123,127,1,127,5,131,2,131,6,135,1,135,2,139,1,139,9,0,99,2,14,0,0';
let pos = 0;
let val1 = 0;
let val2 = 0;
let writePos = 0;
let noun = 0;
let verb = 0;
const solution = 19690720;

console.time('execute');
let inputArr = input.split(',').map(function (x) {
  return parseInt(x, 10);
});

const initialInputArr = inputArr.slice();

const handleOpcodeBlock = (inputArr, pos) => {
  val1 = inputArr[pos + 1];
  val2 = inputArr[pos + 2];
  writePos = inputArr[pos + 3];
  if (inputArr[pos] === 1) {
    let sum = inputArr[val1] + inputArr[val2];
    inputArr[writePos] = sum;
  }

  if (inputArr[pos] === 2) {
    let product = inputArr[val1] * inputArr[val2];
    inputArr[writePos] = product;
  }

  if (inputArr[pos] === 99) {
    pos = inputArr.length;
  }
}

// Find the initial value when both the noun and verb are 0
inputArr[1] = noun;
inputArr[2] = verb;
while (pos < inputArr.length) {
  handleOpcodeBlock(inputArr, pos);
  pos = pos + 4;
}
let initialValue = inputArr[0];

// Calculate the multiplier when we increment the noun by 1
inputArr = initialInputArr.slice();
inputArr[1] = noun + 1;
inputArr[2] = verb;
pos = 0;
while (pos < inputArr.length) {
  handleOpcodeBlock(inputArr, pos);
  pos = pos + 4;
}
let newValue = inputArr[0];

let nounMultiplier = newValue - initialValue;

// Calculate the multiplier when we increment the verb by 1
inputArr = initialInputArr.slice();
inputArr[1] = noun + 1;
inputArr[2] = verb + 1;
pos = 0;
while (pos < inputArr.length) {
  handleOpcodeBlock(inputArr, pos);
  pos = pos + 4;
}

let verbMultiplier = inputArr[0] - newValue;

// Find the larger value and assign it as the multiplier.
// Find the smaller value and assign it as the adder.
let multiplier = 0;
let adder = 0;
if (nounMultiplier > verbMultiplier) {
  multiplier = nounMultiplier;
  adder = verbMultiplier;
} else {
  multiplier = verbMultiplier;
  adder = nounMultiplier;
}

/*
 * Take the initial value and subtract it out of the solution value.
 * Calculation is as follows:
 * multiplierValue (mv) = adjustedSolution (as) / multiplier (m)
 * noun (n) = floor(multiplierValue)
 * remainder (r) = as - (n * m)
 * verb (v) = r / adder (a)
 * Therefore, solution = mn + v
 */
const adjustedSolution = solution - initialValue;
let multiplierValue = adjustedSolution / multiplier;
let finalNoun = Math.floor(multiplierValue);

let remainder = adjustedSolution - (finalNoun * multiplier);
let finalVerb = remainder / adder;
console.timeEnd('execute');
console.log(`Noun multiplier: ${nounMultiplier}`);
console.log(`Verb multiplier: ${verbMultiplier}`);
console.log('final Noun: '+finalNoun);
console.log('final Verb: '+ finalVerb);

// Verify result
inputArr = initialInputArr.slice();
inputArr[1] = finalNoun;
inputArr[2] = finalVerb;
pos = 0;
while (pos < inputArr.length) {
  handleOpcodeBlock(inputArr, pos);
  pos = pos + 4;
}
console.log(`Is our final answer of ${inputArr[0]} equal to ${solution}?`);