const input = '1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,6,27,1,27,5,31,2,6,31,35,1,5,35,39,2,39,9,43,1,43,5,47,1,10,47,51,1,51,6,55,1,55,10,59,1,59,6,63,2,13,63,67,1,9,67,71,2,6,71,75,1,5,75,79,1,9,79,83,2,6,83,87,1,5,87,91,2,6,91,95,2,95,9,99,1,99,6,103,1,103,13,107,2,13,107,111,2,111,10,115,1,115,6,119,1,6,119,123,2,6,123,127,1,127,5,131,2,131,6,135,1,135,2,139,1,139,9,0,99,2,14,0,0';
let pos = 0;
let val1 = 0;
let val2 = 0;
let writePos = 0;

console.time('execute');

const inputArr = input.split(',').map(function (x) {
  return parseInt(x, 10);
});

const handleOptBlock = (inputArr, pos) => {
  val1 = inputArr[pos + 1];
  val2 = inputArr[pos + 2];
  writePos = inputArr[pos + 3];
  if (inputArr[pos] === 1) {
    let sum = inputArr[val1] + inputArr[val2];
    inputArr[writePos] = sum;
  }

  if (inputArr[pos] === 2) {
    let product = inputArr[val1] * inputArr[val2];
    inputArr[writePos] = product;
  }

  if (inputArr[pos] === 99) {
    console.timeEnd('execute');
    console.log(inputArr[0]);
    process.exit(0);
  }
}

inputArr[1] = 12;
inputArr[2] = 2;

while (pos < inputArr.length) {
  handleOptBlock(inputArr, pos);
  pos = pos + 4;
}


