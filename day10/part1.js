const fs = require('fs');
const input = './input/input.txt';
let spaceMap = [];
let asteroids = [];
let resultMap = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const lines = contents.split('\n');
  lines.forEach(line => {
    const row = line.split('');
    spaceMap.push(row);
  });

  // Iterate through the space map looking for each asteroid
  for (let y = 0; y < spaceMap.length; y++) {
    for (let x = 0; x < spaceMap[0].length; x++) {
      const currentPoint = spaceMap[y][x];
      if (currentPoint === '#') {
        // count the points in the line of sight of the asteroid
        asteroids.push({x, y});
      }
    }
  }

  // Count the asteroids in each asteroid's line of sight
  countAsteroids();
  const keys = Object.keys(resultMap);
  let largest = 0;
  let lKey
  keys.forEach(key => {
    if (resultMap[key] > largest) {
      largest = resultMap[key];
      lKey = key;
    }
  });
  console.timeEnd('execute');
  console.log(`Largest value of ${largest} found at ${lKey}.`);
});

const countAsteroids = () => {
  for (let a = 0; a < asteroids.length; a++) {
    for (let b = 0; b < asteroids.length; b++) {
      if (a !== b) {
        compareAsteroids(asteroids[a], asteroids[b]);
      }
    }
  }
}

const compareAsteroids = (asteroidA, asteroidB) => {
  const ax = asteroidA['x'];
  const ay = asteroidA['y'];
  const bx = asteroidB['x'];
  const by = asteroidB['y'];
  let count = 0;

  // If the asteroids are directly across from each other
  if (ax === bx || ay === by) {
    // Case 1: Asteroid B is above A.
    if (by < ay) {
      for (let y = ay; y >= by; y--) {
        if (y === by) {
          count++;
        } else if (spaceMap[y][ax] === '#' && ay !== y) {
          break;
        }
      }
    }

    // Case 2: Asteroid B is below A.
    if (by > ay) {
      for (let y = ay; y <= by; y++) {
        if (y === by) {
          count++;
        } else if (spaceMap[y][ax] === '#' && ay !== y) {
          break;
        }
      }
    }

    // Case 3: Asteroid B is left of Asteroid A
    if (bx < ax) {
      for (let x = ax; x >= bx; x--) {
        if (x === bx) {
          count++;
        } else if (spaceMap[ay][x] === '#' && ax !== x) {
          break;
        }
      }
    }

    // Case 4: Asteroid B is right of Asteroid A
    if (bx > ax) {
      for (let x = ax; x <= bx; x++) {
        if (x === bx) {
          count++;
        } else if (spaceMap[ay][x] === '#' && ax !== x) {
          break;
        }
      }
    }
  }

  // Otherwise, the asteroids are diagnally across from each other
  else {
    const ratio = calcRatio(ax, bx, ay, by);
    const rRatio = reduce(ratio[0], ratio[1]);

    // If the ratio cannot be reduced, then there are no other asteroids in the line of sight
    if (ratio[0] === rRatio[0] && ratio[1] === rRatio[1]) {
      count++;

    // If the ratio could be reduced, we must check for asteroids inbetween
    } else {
      let x = ax;
      let y = ay;
      let equal = false;

      // Case 5: Asteroid B is up and to the left of Asteroid A
      if (bx < ax && by < ay) {
        while (!equal) {
          x = x - rRatio[0];
          y = y - rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 6: Asteroid B is up and to the right of Asteroid A
      if (bx > ax && by < ay) {
        while (!equal) {
          x = x + rRatio[0];
          y = y - rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 7: Asteroid B is down and to the left of Asteroid A
      if (bx < ax && by > ay) {
        while (!equal) {
          x = x - rRatio[0];
          y = y + rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 8: Asteroid B is down and to the right of Asteroid A
      if (bx > ax && by > ay) {
        while (!equal) {
          x = x + rRatio[0];
          y = y + rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }
    }
  }

  if (resultMap[`${ax}, ${ay}`] === undefined) {
    resultMap[`${ax}, ${ay}`] = 0;
  }
  resultMap[`${ax}, ${ay}`] = resultMap[`${ax}, ${ay}`] + count;
}

const calcRatio = (x1, x2, y1, y2) => {
  const x = Math.abs(x1 - x2);
  const y = Math.abs(y1 - y2);
  return [x, y];
}

const reduce = (numerator, denominator) => {
  let gcd = function gcd(a,b){
    return b ? gcd(b, a % b) : a;
  };
  gcd = gcd(numerator, denominator);
  return [numerator/gcd, denominator/gcd];
}