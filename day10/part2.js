const fs = require('fs');
const input = './input/input.txt';
let spaceMap = [];
let asteroids = [];
let resultMap = {};
let lastVaporized = 0;
let vaporizedAsteroids = {};

console.time('execute');
fs.readFile(input, 'utf8', function(err, contents) {
  const lines = contents.split('\n');
  lines.forEach(line => {
    const row = line.split('');
    spaceMap.push(row);
  });

  // Iterate through the space map looking for each asteroid
  for (let y = 0; y < spaceMap.length; y++) {
    for (let x = 0; x < spaceMap[0].length; x++) {
      const currentPoint = spaceMap[y][x];
      if (currentPoint === '#') {
        // count the points in the line of sight of the asteroid
        asteroids.push({x, y});
      }
    }
  }

  // Count the asteroids in each asteroid's line of sight
  countAsteroids();
  const keys = Object.keys(resultMap);
  let largest = 0;
  let lKey
  keys.forEach(key => {
    if (resultMap[key] > largest) {
      largest = resultMap[key];
      lKey = key;
    }
  });

  // Determine the first two points of our three point system.
  const msArr = lKey.split(', ');
  const tPoint = {'x': parseInt(msArr[0]), 'y': 0};
  const msPoint = {'x': parseInt(msArr[0]), 'y': parseInt(msArr[1])};

  // Iterate through all of the asteroids until they have all been vaporized
  let allVaporized = false;
  while (!allVaporized) {
    let angles = {};
    let lastAngle = null;
    asteroids.forEach(asteroid => {
      const angle = calculateAngle(msPoint, tPoint, asteroid);
      if (!isNaN(angle)) {
        if (angles[angle] === undefined) {
          angles[angle] = [];
        }
        angles[angle].push(asteroid);
      }
    });

    // Iterate through each asteroid by angle
    const keys = Object.keys(angles).sort((a, b) => a - b);

    keys.forEach((key, index) => {
      let asteroidArr = angles[key];
      let curAst;
      if (asteroidArr.length === 1) {
        curAst = asteroidArr[0];
      } else {
        let distance = 99999;
        asteroidArr.forEach(ast => {
          let dist = mDist(msPoint.x, msPoint.y, ast.x, ast.y);
          if (dist < distance) {
            curAst = ast;
            distance = dist;
          }
        });
      }

      const i = asteroids.indexOf(curAst);
      asteroids.splice(i, 1);
      vaporizedAsteroids[++lastVaporized] = curAst;
    });

    if (asteroids.length === 1) {
      allVaporized = true;
    }
  }

  console.timeEnd('execute');
  console.log(vaporizedAsteroids);
});

const countAsteroids = () => {
  for (let a = 0; a < asteroids.length; a++) {
    for (let b = 0; b < asteroids.length; b++) {
      if (a !== b) {
        compareAsteroids(asteroids[a], asteroids[b]);
      }
    }
  }
}

const compareAsteroids = (asteroidA, asteroidB) => {
  const ax = asteroidA['x'];
  const ay = asteroidA['y'];
  const bx = asteroidB['x'];
  const by = asteroidB['y'];
  let count = 0;

  // If the asteroids are directly across from each other
  if (ax === bx || ay === by) {
    // Case 1: Asteroid B is above A.
    if (by < ay) {
      for (let y = ay; y >= by; y--) {
        if (y === by) {
          count++;
        } else if (spaceMap[y][ax] === '#' && ay !== y) {
          break;
        }
      }
    }

    // Case 2: Asteroid B is below A.
    if (by > ay) {
      for (let y = ay; y <= by; y++) {
        if (y === by) {
          count++;
        } else if (spaceMap[y][ax] === '#' && ay !== y) {
          break;
        }
      }
    }

    // Case 3: Asteroid B is left of Asteroid A
    if (bx < ax) {
      for (let x = ax; x >= bx; x--) {
        if (x === bx) {
          count++;
        } else if (spaceMap[ay][x] === '#' && ax !== x) {
          break;
        }
      }
    }

    // Case 4: Asteroid B is right of Asteroid A
    if (bx > ax) {
      for (let x = ax; x <= bx; x++) {
        if (x === bx) {
          count++;
        } else if (spaceMap[ay][x] === '#' && ax !== x) {
          break;
        }
      }
    }
  }

  // Otherwise, the asteroids are diagnally across from each other
  else {
    const ratio = calcRatio(ax, bx, ay, by);
    const rRatio = reduce(ratio[0], ratio[1]);

    // If the ratio cannot be reduced, then there are no other asteroids in the line of sight
    if (ratio[0] === rRatio[0] && ratio[1] === rRatio[1]) {
      count++;

    // If the ratio could be reduced, we must check for asteroids inbetween
    } else {
      let x = ax;
      let y = ay;
      let equal = false;

      // Case 5: Asteroid B is up and to the left of Asteroid A
      if (bx < ax && by < ay) {
        while (!equal) {
          x = x - rRatio[0];
          y = y - rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 6: Asteroid B is up and to the right of Asteroid A
      if (bx > ax && by < ay) {
        while (!equal) {
          x = x + rRatio[0];
          y = y - rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 7: Asteroid B is down and to the left of Asteroid A
      if (bx < ax && by > ay) {
        while (!equal) {
          x = x - rRatio[0];
          y = y + rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }

      // Case 8: Asteroid B is down and to the right of Asteroid A
      if (bx > ax && by > ay) {
        while (!equal) {
          x = x + rRatio[0];
          y = y + rRatio[1];
          if (x === bx && y === by) {
            count++;
            equal = true;
          } else if (spaceMap[y][x] === '#') {
            equal = true;
          }
        }
      }
    }
  }

  if (resultMap[`${ax}, ${ay}`] === undefined) {
    resultMap[`${ax}, ${ay}`] = 0;
  }
  resultMap[`${ax}, ${ay}`] = resultMap[`${ax}, ${ay}`] + count;
}

const calcRatio = (x1, x2, y1, y2) => {
  const x = Math.abs(x1 - x2);
  const y = Math.abs(y1 - y2);
  return [x, y];
}

const reduce = (numerator, denominator) => {
  let gcd = function gcd(a,b){
    return b ? gcd(b, a % b) : a;
  };
  gcd = gcd(numerator, denominator);
  return [numerator/gcd, denominator/gcd];
}

const calculateAngle = (a, b, c) => {
  // Find the length of each vector in the system
  const ac = calculateLength(a, c);
  const ab = ac;
  b.y = a.y - ac;
  const bc = calculateLength(b, c);

  // Determine if c is to the left or right of a
  let left = false;
  let below = false;
  if (c.x < a.x) {
    left = true;
  }
  if (c.y > a.y) {
    below = true;
  }

  // Calculate the angle between them using arccos
  const numerator = Math.pow(ab, 2) + Math.pow(ac, 2) - Math.pow(bc, 2);
  const denominator = 2 * ab * ac;
  const radians = Math.acos(numerator/denominator);
  const pi = Math.PI;
  let angle = parseFloat((radians * (180/pi)).toPrecision(8));

  // Work with all 360 degrees during the rotation
  if (angle === 0) {
    if (below === true) {
      angle = 180;
    } else {
      angle = 0;
    }
  } else {
    if (left === true) {
      angle = 360 - angle;
    }
    angle = parseFloat(angle.toPrecision(8));
  }

  return angle;
}

const calculateLength = (a, b) => {
  const xDiff = Math.pow(a['x'] - b['x'], 2);
  const yDiff = Math.pow(a['y'] - b['y'], 2)
  const length = Math.sqrt(xDiff + yDiff);

  return length;
}

let mDist = function(x1, y1, x2, y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}
